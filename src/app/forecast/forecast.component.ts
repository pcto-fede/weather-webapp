import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-forecast',
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.scss'],
})
export class ForecastComponent implements OnInit {
  @Input() city!: any;
  @Input() oneCall!: any;
  currentDate = new Date();
  forecastDays = new Array(7);
  constructor() {
    for (let i = 1; i <= 7; i++) {
      this.forecastDays[i - 1] = this.currentDate.getDate() + i;
    }
  }

  ngOnInit(): void {}
}
