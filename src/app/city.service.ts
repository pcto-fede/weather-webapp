import { HttpClient } from '@angular/common/http';
import { SAMPLECITY } from './models/sample-city';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { City } from './models/city';

@Injectable({
  providedIn: 'root',
})
export class CityService {
  private city: City = SAMPLECITY;
  private apiKey = 'dc45f3687e4aa405fe5b272b080cbf03';
  private cityJson: any;
  private oneCallJson: any;

  getCity(): Observable<any> {
    return of(this.cityJson);
  }

  setCity(name: string): Observable<any> {
    let response = this.http.get(
      `http://api.openweathermap.org/data/2.5/weather?q=${name}&appid=${this.apiKey}&units=metric&lang=it`
    );
    response.subscribe((data) => (this.cityJson = data));
    return response;
  }
  setCityById(name: number): Observable<any> {
    let response = this.http.get(
      `http://api.openweathermap.org/data/2.5/weather?id=${name}&appid=${this.apiKey}&units=metric&lang=it`
    );
    response.subscribe((data) => (this.cityJson = data));
    return response;
  }

  setCityOneCall(lat: number, lon: number): Observable<any> {
    let response = this.http.get(
      `http://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lon}&appid=${this.apiKey}&units=metric&lang=it`
    );
    response.subscribe((data) => (this.oneCallJson = data));
    return response;
  }
  getCityOneCall(): Observable<any> {
    return of(this.oneCallJson);
  }

  searchCity(term: string): Observable<any> {
    let response = this.http.get(
      `https://api.geoapify.com/v1/geocode/autocomplete?text=${term}&apiKey=cce36d31475b4c1c8058cee8f53f2996`
    );
    return response;
  }

  constructor(private http: HttpClient) {}
}
