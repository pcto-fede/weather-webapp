import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpClientModule } from '@angular/common/http';

//PRIMENG modules
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { CardModule } from 'primeng/card';
import { Splitter, SplitterModule } from 'primeng/splitter';
import { MenubarModule } from 'primeng/menubar';
import { MenuItem } from 'primeng/api';
import { ChartModule } from 'primeng/chart';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CityDetailComponent } from './city-detail/city-detail.component';
import { CityPreviewComponent } from './city-preview/city-preview.component';
import { CitySearchComponent } from './city-search/city-search.component';
import { ForecastComponent } from './forecast/forecast.component';
import { MapComponent } from './map/map.component';
import { FrcGraphComponent } from './frc-graph/frc-graph.component';

@NgModule({
  declarations: [
    AppComponent,
    CityDetailComponent,
    CityPreviewComponent,
    CitySearchComponent,
    ForecastComponent,
    MapComponent,
    FrcGraphComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    InputTextModule,
    ButtonModule,
    CardModule,
    SplitterModule,
    MenubarModule,
    ChartModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
