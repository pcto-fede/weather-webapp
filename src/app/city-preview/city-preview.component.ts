import { CityService } from './../city.service';
import { Component, OnInit } from '@angular/core';
import { City } from '../models/city';
import { Input } from '@angular/core';
@Component({
  selector: 'app-city-preview',
  templateUrl: './city-preview.component.html',
  styleUrls: ['./city-preview.component.scss'],
})
export class CityPreviewComponent implements OnInit {
  @Input() city: any;
  @Input() oneCall: any;
  @Input() currentDate!: Date;
  constructor(private cityService: CityService) {}

  ngOnInit(): void {
    // console.log('cityName: ' + this.cityName);
    // this.cityService
    //   .setCity(this.cityName)
    //   .subscribe((data) => (this.city = data));
    // this.cityService
    //   .setCityOneCall(this.cityName)
    //   .subscribe((data) => (this.city = data));
  }
}
