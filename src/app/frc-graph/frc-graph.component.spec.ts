import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FrcGraphComponent } from './frc-graph.component';

describe('FrcGraphComponent', () => {
  let component: FrcGraphComponent;
  let fixture: ComponentFixture<FrcGraphComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FrcGraphComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FrcGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
