import { Observable } from 'rxjs';
import { CityStorageService } from './../city-storage.service';
import { Component, OnInit, NgModule } from '@angular/core';
import { Input } from '@angular/core';
@Component({
  selector: 'app-frc-graph',
  templateUrl: './frc-graph.component.html',
  styleUrls: ['./frc-graph.component.scss'],
})
export class FrcGraphComponent implements OnInit {
  @Input() city?: any;
  @Input() oneCall?: any;
  obCity: Observable<any>;
  obOneCall: Observable<any>;
  currentDate = new Date();
  forecastDays = new Array(7);
  labels: string[] = new Array(7);

  data: any = {
    labels: ['arstarst', 'February', 'March', 'April', 'May', 'June', 'July'],
    datasets: [
      {
        label: 'Temp',
        fill: false,
        borderColor: '#42A5F5',
        yAxisID: 'y',
        tension: 0.4,
        data: [65, 59, 80, 81, 56, 55, 40],
      },
      {
        label: 'Hum',
        fill: false,
        borderColor: '#00bb7e',

        yAxisID: 'y2',
        tension: 0.4,
        data: [65, 59, 80, 81, 56, 55, 40],
      },
      {
        label: 'Wind',
        fill: false,
        borderColor: '#FFA726',
        yAxisID: 'y3',
        tension: 0.4,
        data: [65, 59, 80, 81, 56, 55, 40],
      },
    ],
  };

  // axisOption: any = {
  //   stacked: false,
  //   scales: {
  //     scales: {
  //       x: {
  //         ticks: {
  //           color: '#495057',
  //         },
  //         grid: {
  //           color: '#ebedef',
  //         },
  //       },
  //       y: {
  //         type: 'linear',
  //         display: true,
  //         position: 'left',
  //         ticks: {
  //           color: '#42A5F5',
  //         },
  //         grid: {
  //           color: '#ebedef',
  //         },
  //       },
  //       y1: {
  //         type: 'linear',
  //         display: true,
  //         position: 'right',
  //         ticks: {
  //           color: '#00bb7e',
  //         },
  //         grid: {
  //           drawOnChartArea: false,
  //           color: '#ebedef',
  //         },
  //       },
  //       y2: {
  //         type: 'linear',
  //         display: true,
  //         position: 'right',
  //         ticks: {
  //           color: '#FFA726',
  //         },
  //         grid: {
  //           drawOnChartArea: false,
  //           color: '#ebedef',
  //         },
  //       },
  //     },
  //   },
  // };

  update() {
    let tempData: number[] = new Array(7);
    let humData: number[] = new Array(7);
    let windData: number[] = new Array(7);

    for (let i = 0; i < 7; i++) {
      tempData[i] = this.oneCall.daily[i + 1].temp.day;
      humData[i] = this.oneCall.daily[i + 1].humidity;
      windData[i] = this.oneCall.daily[i + 1].wind_speed;
    }

    this.data = {
      labels: this.labels,
      datasets: [
        {
          label: 'Temp',
          fill: false,
          borderColor: '#42A5F5',
          yAxisID: 'y',
          tension: 0.4,
          data: tempData,
        },
        {
          label: 'Hum',
          fill: false,
          borderColor: '#00bb7e',

          yAxisID: 'y1',
          tension: 0.4,
          data: humData,
        },
        {
          label: 'Wind',
          fill: false,
          borderColor: '#FFA726',
          yAxisID: 'y2',
          tension: 0.4,
          data: windData,
        },
      ],
    };
  }

  constructor(private cityStorageService: CityStorageService) {
    for (let i = 1; i <= 7; i++) {
      this.forecastDays[i - 1] = this.currentDate.getDate() + i;
      this.labels[i - 1] =
        this.currentDate.getMonth() + 1 + '/' + this.forecastDays[i - 1];
    }
    this.obCity = this.cityStorageService.getObCity();
    this.obOneCall = this.cityStorageService.getObOneCall();
    this.obOneCall.subscribe((data) => {
      this.oneCall = data;
      // this.oneCall = this.cityStorageService.getOneCall;
      this.update();
    });
  }

  ngOnInit(): void {}
}
