import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { Map } from 'mapbox-gl';
import * as mapboxgl from 'mapbox-gl';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
})
export class MapComponent implements OnInit {
  map!: Map;
  // style = 'mapbox://styles/mapbox/streets-v11';
  style = 'mapbox://styles/cabat/ckpfqd1em0knf18m4bhab0krp';
  @Input() lat = 45;
  @Input() lon = 9;
  constructor() {}
  ngOnInit() {
    (mapboxgl as any).accessToken =
      'pk.eyJ1IjoiY2FiYXQiLCJhIjoiY2twZnE4cnVxMHlzMTJxbzhleWd2a2s3cCJ9.qJrhjX0n_jN7gS2oTuNXkw';
    this.map = new mapboxgl.Map({
      container: 'map',
      style: this.style,
      zoom: 9,
      center: [this.lon, this.lat],
    }); // Add map controls
    //this.map.addControl(new mapboxgl.NavigationControl());
  }
}
