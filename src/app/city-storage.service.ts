import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class CityStorageService {
  private oneCall: any;
  private city: any = { id: 0 };
  private obOneCall!: Observable<any>;
  private obCity!: Observable<any>;
  public stored = false;
  private bookmarksId: number[] = [];
  private bookmarksName: string[] = [];

  storeCity(city: any): void {
    this.stored = true;
    this.city = city;
  }

  storeObCity(obCity: Observable<any>): void {
    this.obCity = obCity;
  }

  storeOneCall(oneCall: any): void {
    this.stored = true;
    this.oneCall = oneCall;
  }

  storeObOneCall(obOneCall: Observable<any>): void {
    this.obOneCall = obOneCall;
  }

  getCity(): any {
    return this.city;
  }

  getObCity(): Observable<any> {
    return this.obCity;
  }

  getOneCall(): any {
    return this.oneCall;
  }

  getObOneCall(): Observable<any> {
    return this.obOneCall;
  }

  // bookmarks
  addBookmark(name: string, id: number) {
    console.log(`adding bookmark ${id}`);
    if (this.bookmarksId.indexOf(id) != -1) {
      console.log('bookmark already saved');
    } else {
      this.bookmarksId.push(id);
      this.bookmarksName.push(name);
      localStorage.setItem('bookmarksId', JSON.stringify(this.bookmarksId));
      localStorage.setItem('bookmarksName', JSON.stringify(this.bookmarksName));
      console.log('bookmarks saved');
    }
  }

  removeBookmark(id: number): boolean {
    console.log(`removing bookmark ${id}`);
    let index = this.bookmarksId.indexOf(id);
    if (index == -1) {
      console.log('bookmark not found');
    } else {
      // this.bookmarks = this.bookmarks.filter((item) => item != id);
      this.bookmarksId = this.bookmarksId
        .slice(0, index)
        .concat(this.bookmarksId.slice(index + 1, this.bookmarksId.length));
      this.bookmarksName = this.bookmarksName
        .slice(0, index)
        .concat(this.bookmarksName.slice(index + 1, this.bookmarksName.length));
      localStorage.setItem('bookmarksId', JSON.stringify(this.bookmarksId));
      localStorage.setItem('bookmarksName', JSON.stringify(this.bookmarksName));
      console.log('bookmarks removed');
    }
    return false;
  }

  getBookmarks(): any[] {
    if (JSON.parse(localStorage.getItem('bookmarksId') as string)) {
      this.bookmarksId = JSON.parse(
        localStorage.getItem('bookmarksId') as string
      );
    }

    if (JSON.parse(localStorage.getItem('bookmarksName') as string)) {
      this.bookmarksName = JSON.parse(
        localStorage.getItem('bookmarksName') as string
      );
    }

    let ret = new Array(this.bookmarksId.length);
    for (let i = 0; i < this.bookmarksId.length; i++) {
      ret[i] = { name: this.bookmarksName[i], id: this.bookmarksId[i] };
    }
    return ret;
  }
  constructor() {}
}
