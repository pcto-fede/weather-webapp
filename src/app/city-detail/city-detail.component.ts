import { CityStorageService } from './../city-storage.service';
import { Component, OnInit } from '@angular/core';
import { City } from '../models/city';
import { Input } from '@angular/core';
import { CityService } from '../city.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-city-detail',
  templateUrl: './city-detail.component.html',
  styleUrls: ['./city-detail.component.scss'],
})
export class CityDetailComponent implements OnInit {
  cityId = Number(this.route.snapshot.paramMap.get('name'));
  city!: any;
  oneCall!: any;
  currentDate = new Date();
  forecastDays = new Array(7);

  multi: any[] = new Array(7);

  isFrcDisplayed = true;
  toggleGraph(): void {
    this.isFrcDisplayed = !this.isFrcDisplayed;
  }
  goBack(): void {
    this.location.back();
  }

  constructor(
    private cityService: CityService,
    private cityStorageService: CityStorageService,
    private route: ActivatedRoute,
    private location: Location
  ) {
    // this.forecastDays[0] = this.currentDate.getDate();
    for (let i = 1; i <= 7; i++) {
      this.forecastDays[i - 1] = this.currentDate.getDate() + i;
    }
  }

  ngOnInit(): void {
    console.log('cityId: ' + this.cityId);

    this.city = this.cityStorageService.getCity();
    if (this.city.id == this.cityId) {
      this.oneCall = this.cityStorageService.getOneCall();
      console.log('restored');
    } else {
      let cityResponse = this.cityService.setCityById(this.cityId);
      this.cityStorageService.storeObCity(cityResponse);
      cityResponse.subscribe((data) => {
        this.city = data;
        let oneCallResponse = this.cityService.setCityOneCall(
          this.city.coord.lat,
          this.city.coord.lon
        );
        this.cityStorageService.storeObOneCall(oneCallResponse);
        oneCallResponse.subscribe((data) => {
          this.oneCall = data;
          this.cityStorageService.storeCity(this.city);
          this.cityStorageService.storeOneCall(this.oneCall);
          console.log('called');
        });
      });
    }
  }
}
