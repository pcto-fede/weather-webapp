import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { CityStorageService } from './../city-storage.service';
import { Component, OnInit } from '@angular/core';
import { CityService } from '../city.service';

@Component({
  selector: 'app-city-search',
  templateUrl: './city-search.component.html',
  styleUrls: ['./city-search.component.scss'],
})
export class CitySearchComponent implements OnInit {
  cityName = 'Milano';
  city!: any;
  oneCall!: any;

  currentDate: Date = new Date();

  searchTerm?: string;
  searchedTerms$!: Observable<any>;
  searchTerms = new Subject<string>();

  search(term: string): void {
    this.searchTerms.next(term);
  }

  searchCity() {
    if (this.searchTerm) {
      this.cityName = this.searchTerm;
    }

    this.cityService.setCity(this.cityName).subscribe((data) => {
      this.city = data;
      this.cityService
        .setCityOneCall(this.city.coord.lat, this.city.coord.lon)
        .subscribe((data) => (this.oneCall = data));
    });
  }

  constructor(
    private cityService: CityService,
    public cityStorageService: CityStorageService
  ) {}

  ngOnInit(): void {
    // console.log('cityName: ' + this.cityName);

    this.city = this.cityStorageService.getCity();
    this.oneCall = this.cityStorageService.getOneCall();

    if (!this.cityStorageService.stored) {
      console.log('called');
      let cityResponse = this.cityService.setCity(this.cityName);
      this.cityStorageService.storeObCity(cityResponse);
      cityResponse.subscribe((data) => {
        this.city = data;

        let oneCallResponse = this.cityService.setCityOneCall(
          this.city.coord.lat,
          this.city.coord.lon
        );
        this.cityStorageService.storeObOneCall(oneCallResponse);
        oneCallResponse.subscribe((data) => {
          this.oneCall = data;
          this.cityStorageService.storeCity(this.city);
          this.cityStorageService.storeOneCall(data);
        });
      });
    }

    this.searchedTerms$ = this.searchTerms.pipe(
      debounceTime(600),
      distinctUntilChanged(),
      switchMap((term) => this.cityService.searchCity(term))
    );
  }
}
