import { City } from './city';

export const SAMPLECITY: City = {
  name: 'Milano',
  lat: '1',
  lon: '2',
  currentTemp: 21,
};
