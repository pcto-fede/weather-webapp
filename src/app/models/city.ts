export interface City {
  name: string;
  lat: string;
  lon: string;
  //TODO: add more weather data
  currentTemp: number;
}
